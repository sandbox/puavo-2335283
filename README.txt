INTRODUCTION
------------
Simple REST API offers Drupal 8 style REST API for reading, creating, updating
and deleting nodes. You can make your systems to support Drupal 8 before
actually updating Drupal
* Project page: https://www.drupal.org/sandbox/puavo/2335283

REQUIREMENTS
------------
No special requirements.

INSTALLATION
------------
Install as you would normally install a contributed drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-7
for further information.

CONFIGURATION
------------- 
* Configure user permissions in Administration » People » Permissions:
 - There's a permission for GET, POST, PATCH, and DELETE requests.
 - In addition to those permission, user must have corresponding permission to
   read, create, update, or delete nodes in Drupal to use API.
