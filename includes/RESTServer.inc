<?php

/**
 * @file
 * Class for handling REST calls.
 */

class SimpleRestApiRESTServer {
  /* @var $context RequestDataInterface */
  protected $context;

  /**
   * Constructor.
   */
  public function __construct(SimpleRestApiRequestDataInterface $context) {
    $this->context = $context;
  }

  /**
   * Handles the call to the REST server.
   */
  public function handle() {
    // Check that path is acceptable.
    $path_array = $this->getCanonicalPathArray();
    if (empty($path_array) || $path_array[0] != 'node') {
      drupal_add_http_header('Status', '406 Not Acceptable.');
      return;
    }

    // Read the body of HTTP request.
    // If method is not POST or PATCH body_data will be empty string.
    $body_data = $this->getRequestBody();
    // Transform body from JSON to an object.
    $body_data = json_decode($body_data, FALSE);

    // Check that JSON is valid.
    $json_error = json_last_error();
    if ($json_error != JSON_ERROR_NONE) {
      drupal_add_http_header('Status', '400 Bad Request: Invalid Request body');
      return;
    }

    $method = $this->getRequestMethod();
    $nid = $path_array[1];
    // Check that we have node id if needed.
    if (!$nid && $method != 'POST') {
      drupal_add_http_header('Status', '400 Bad Request: Node id not defined');
      return;
    }

    switch ($method) {
      case 'GET':
        $node = node_load($nid);
        if (!$node) {
          drupal_add_http_header('Status', '404 Not Found');
          break;
        }
        if (node_access('view', $node)) {
          drupal_json_output($node);
        }
        else {
          drupal_access_denied();
        }
        break;

      case 'POST':
        if (isset($node->_links->type->href)) {
          drupal_add_http_header('Status', '400 Bad Request');
          echo '{"error":"The type link relation must be specified."}';
          break;
        }
        // Create new node.
        $node = new stdClass();
        $this->parseRequestBody($node, $body_data);
        $type = $node->type;

        if (node_access('create', $type)) {
          // Set defaults.
          node_object_prepare($node);
          // Save node.
          node_save($node);
          // Success.
          drupal_add_http_header('Status', '201 Created');
        }
        else {
          drupal_add_http_header('Status', '403 Forbidden');
        }
        break;

      case 'PATCH':
        $node = node_load($nid);
        if (!$node) {
          drupal_add_http_header('Status', '404 Not Found');
          break;
        }
        if (node_access('update', $node)) {
          $this->parseRequestBody($node, $body_data);
          node_save($node);
          drupal_add_http_header('Status', '204 No Content');
        }
        else {
          drupal_add_http_header('Status', '403 Forbidden');
        }
        break;

      case 'DELETE':
        $node = node_load($nid);
        if (!$node) {
          drupal_add_http_header('Status', '404 Not Found');
          break;
        }
        if (node_access('delete', $node)) {
          node_delete($nid);
          drupal_add_http_header('Status', '204 No Content');
        }
        else {
          drupal_add_http_header('Status', '403 Forbidden');
        }
        break;

      default:
        // Method is not supprted.
        drupal_add_http_header('Status', '405 Method Not Allowed');
        break;
    }
  }

  /**
   * Parse data from request body to node.
   *
   * @param Object $node
   *   Node object.
   * @param Object $body_data
   *   Request body in object form.
   */
  public function parseRequestBody(Object $node, Object $body_data) {
    // Handle system fields
    /* Check language of the node. If it is not found use
    LANGUANE_NONE for default.*/
    if (isset($body_data->langcode)) {
      $language = $body_data->langcode[0]->value;
    }
    elseif (isset($node->language)) {
      $language = $node->language;
    }
    else {
      $language = LANGUAGE_NONE;
    }
    $node->language = $language;

    // Parse type from _links. Just ignore possible type field.
    $type_uri = $body_data->_links->type->href;
    $uri_parts = explode('/', $type_uri);
    $type = array_pop($uri_parts);
    $node->type = $type;

    if (isset($body_data->status)) {
      $node->status = $body_data->status[0]->value;
    }
    if (isset($body_data->promote)) {
      $node->promote = $body_data->promote[0]->value;
    }
    if (isset($body_data->comment)) {
      $node->comment = $body_data->comment[0]->value;
    }
    if (isset($body_data->sticky)) {
      $node->sticky = $body_data->sticky[0]->value;
    }
    if (isset($body_data->title)) {
      $node->title = $body_data->title[0]->value;
    }
    if ($body_data->body) {
      $field_array[0] = array(
        'value' => $body_data->body[0]->value,
      );
      $node->body = array($language => $field_array);
    }

    // Handle regular node fields from drupal 8 node style to drupal 7.
    foreach ($body_data as $field_name => $field_values) {
      if (is_array($field_values) && substr($field_name, 0, 5) === 'field') {
        $field_array = array();
        foreach ($field_values as $key => $value) {
          $field_array[$key] = array('value' => $value->value);
        }
        $node->$field_name = array($language => $field_array);
      }
    }

    // Handle entity, term and file references (embedded resources).
    foreach ($body_data->_embedded as $uri => $resource) {
      // Parse name of the field from $uri.
      $uri_parts = explode('/', $uri);
      $field_name = array_pop($uri_parts);
      $field_array = array();
      foreach ($resource as $key => $value) {
        // Parse id and type.
        $reference_uri = $value->_links->self->href;
        $uri_parts = explode('/', $reference_uri);
        $reference_id = array_pop($uri_parts);
        $reference_type = array_pop($uri_parts);

        if ($reference_type == 'term') {
          // Save term id to reference field.
          $field_array[$key] = array('tid' => $reference_id);
        }
        elseif ($reference_type == 'node') {
          // Save node id to reference field.
          $field_array[$key] = array('target_id' => $reference_id);
        }
        elseif ($reference_type == 'file') {
          // Save file id to reference field.
          $field_array[$key] = array('fid' => $reference_id);
        }
      }
      $node->$field_name = array($language => $field_array);
    }
  }

  /**
   * Canonical path is the url of the request without path of endpoint.
   *
   * For example endpoint has path 'rest'. Canonical of request to url
   * 'rest/node/1.php' will be 'node/1.php'.
   *
   * @return string
   *   Canonical path
   */
  public function getCanonicalPath() {
    // Use drupal_static so we can clear this static cache during unit testing.
    $canonical_path = &drupal_static('RESTServerGetCanonicalPath');
    if (empty($canonical_path)) {
      $canonical_path = $this->context->getCanonicalPath();
    }
    return $canonical_path;
  }

  /**
   * Explode canonical path to parts by '/'.
   *
   * @return array
   *   Canonical path parts in array
   */
  protected function getCanonicalPathArray() {
    $canonical_path = $this->getCanonicalPath();
    $canonical_path_array = explode('/', $canonical_path);

    return $canonical_path_array;
  }

  /**
   * Return name of resource.
   *
   * Example. We have endpoint with path 'rest'.
   * Request is done to url /rest/node/1.php'.
   * Name of resource in this case is 'node'.
   *
   * @return string
   *   Name of resource.
   */
  protected function getResourceName() {
    $canonical_path_array = $this->getCanonicalPathArray();
    $resource_name = array_shift($canonical_path_array);

    return $resource_name;
  }

  /**
   * Determine the request method.
   */
  protected function getRequestMethod() {
    return $this->context->getRequestMethod();
  }

  /**
   * Get body of the request.
   */
  protected function getRequestBody() {
    return $this->context->getRequestBody();
  }
}
