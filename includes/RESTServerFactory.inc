<?php

/**
 * @file
 * Factory class to build RESTServer object.
 */

class SimpleRestApiRESTServerFactory {
  protected $data = array();
  protected static $className = 'SimpleRestApiRESTServer';

  /**
   * We need data property to pass additional arguments to methods.
   *
   * Required property is 'endpoint_path' -- base path of endpoint.
   * Example: GET rest/node/1.php -- "rest" is endpoint path.
   */
  public function __construct($data = array()) {
    if (!isset($data['endpoint_path'])) {
      throw new Exception('RESTServerFactory constructor requires "endpoint_data" property.');
    }
    $this->data = $data;
  }

  /**
   * Create new RESTServer object.
   */
  public function getRESTServer() {
    $context = $this->getContext();

    $class_name = static::$className;
    return new $class_name($context);
  }

  /**
   * Create new RequestData object.
   */
  protected function getContext() {
    $context = new SimpleRestApiRequestData($this->data['endpoint_path']);
    $context->buildFromGlobals();
    return $context;
  }
}
